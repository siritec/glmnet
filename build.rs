use std::process::Command;

#[cfg(target_os = "linux")]
mod glm {
    pub const ARGS: &[&str] = &["-m64","-fdefault-real-8","-ffixed-form","-shared","-O3","-fPIC", "src/glmnet5.f90", "-o", "libglmnet.so"];
}

fn main() {
    Command::new("gfortran")
        .args(glm::ARGS)
        .status()
        .expect("unable to compile fortran library");
    //println!("cargo:rustc-link-search={}", dir.display());
    println!("cargo:rustc-link-lib=glmnet");
}
