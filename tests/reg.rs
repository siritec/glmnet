extern crate glmnet;
#[macro_use]
extern crate ndarray;
extern crate rand;

use glmnet::{GLMNet, GLMNetLR};
use ndarray::Array2;
use rand::{rngs::SmallRng, FromEntropy};
use rand::distributions::{Distribution, StandardNormal};

#[test]
fn test_glmnet_lr() {

    let mut small = SmallRng::from_entropy();
    let a = Array2::from_shape_fn((1, 800_000), |_| StandardNormal.sample(&mut small));
    assert_eq!(a.dim(), (1, 800_000));

    let x = Array2::from_shape_fn((3, 800_000), |_| StandardNormal.sample(&mut small));
    let y: Array2<f64> = &x.slice(s![0..1, ..]) * 0.2 + &x.slice(s![1..2, ..]) * 0.1 + &a;


    let mut glr = GLMNetLR::new(800_000, 3);
    let p = glr.pmax(2).fit(y.as_slice().unwrap(), x.as_slice().unwrap());
    
    let l = p.betas.len();
    assert!((p.betas[l - 3] - 0.2).abs() < 0.01);
    assert!((p.betas[l - 2] - 0.1).abs() < 0.01);
    assert_eq!(p.betas[l - 1],  0.0);
}
