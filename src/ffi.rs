use libc::{c_double, c_int};

extern "C" {
    pub fn elnet_(
        ka: *const c_int,
        parm: *const c_double,
        no: *const c_int,
        ni: *const c_int,
        x: *const c_double,
        y: *const c_double,
        w: *const c_double,
        jd: *const c_int,
        vp: *const c_double,
        cl: *const c_double,
        ne: *const c_int,
        nx: *const c_int,
        nlam: *const c_int,
        flmin: *const c_double,
        ulam: *const c_double,
        thr: *const c_double,
        isd: *const c_int,
        intr: *const c_int,
        maxit: *const c_int,
        lmu: *mut c_int,
        a0: *mut c_double,
        ca: *mut c_double,
        ia: *mut c_int,
        nin: *mut c_int,
        rsq: *mut c_double,
        alm: *mut c_double,
        nlp: *mut c_int,
        jerr: *mut c_int,
    );

    pub fn solns_(
        ni: *const c_int,
        nx: *const c_int,
        lmu: *const c_int,
        ca: *const c_double,
        ia: *const c_int,
        nin: *const c_int,
        b: *mut c_double,
    );
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
